var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { IonicNativePlugin, cordova } from '@ionic-native/core';
var BizMOBPushOriginal = /** @class */ (function (_super) {
    __extends(BizMOBPushOriginal, _super);
    function BizMOBPushOriginal() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BizMOBPushOriginal.prototype.getPushKey = function (param) { return cordova(this, "getPushKey", {}, arguments); };
    BizMOBPushOriginal.prototype.pushRegistration = function (param) { return cordova(this, "pushRegistration", {}, arguments); };
    BizMOBPushOriginal.prototype.pushAlarmSettingInfo = function (param) { return cordova(this, "pushAlarmSettingInfo", {}, arguments); };
    BizMOBPushOriginal.prototype.pushUpdateAlarmSetting = function (param) { return cordova(this, "pushUpdateAlarmSetting", {}, arguments); };
    BizMOBPushOriginal.prototype.pushGetMessages = function (param) { return cordova(this, "pushGetMessages", {}, arguments); };
    BizMOBPushOriginal.prototype.pushMarkAsRead = function (param) { return cordova(this, "pushMarkAsRead", {}, arguments); };
    BizMOBPushOriginal.prototype.pushGetUnreadMessageCount = function (param) { return cordova(this, "pushGetUnreadMessageCount", {}, arguments); };
    BizMOBPushOriginal.prototype.setBadgeCount = function (param) { return cordova(this, "setBadgeCount", {}, arguments); };
    BizMOBPushOriginal.prototype.sendMessages = function (param) { return cordova(this, "sendMessages", {}, arguments); };
    BizMOBPushOriginal.prototype.pushReceivedCheck = function (param) { return cordova(this, "pushReceivedCheck", {}, arguments); };
    BizMOBPushOriginal.prototype.getPushMessageTypeList = function (param) { return cordova(this, "getPushMessageTypeList", {}, arguments); };
    BizMOBPushOriginal.prototype.getPushAgreementInfo = function (param) { return cordova(this, "getPushAgreementInfo", {}, arguments); };
    BizMOBPushOriginal.prototype.updatePushAgreementInfo = function (param) { return cordova(this, "updatePushAgreementInfo", {}, arguments); };
    BizMOBPushOriginal.prototype.on = function (param) { return cordova(this, "on", {}, arguments); };
    BizMOBPushOriginal.pluginName = "BizMOBPush";
    BizMOBPushOriginal.plugin = "bizmob-plugin-push";
    BizMOBPushOriginal.pluginRef = "BizMOBPush";
    BizMOBPushOriginal.repo = "";
    BizMOBPushOriginal.install = "";
    BizMOBPushOriginal.installVariables = [];
    BizMOBPushOriginal.platforms = ["Android", "iOS"];
    return BizMOBPushOriginal;
}(IonicNativePlugin));
var BizMOBPush = new BizMOBPushOriginal();
export { BizMOBPush };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2Jpem1vYi1wbHVnaW4tcHVzaC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBQ0EsT0FBTyw4QkFBc0MsTUFBTSxvQkFBb0IsQ0FBQzs7SUFheEMsOEJBQWlCOzs7O0lBRTdDLCtCQUFVLGFBQUMsS0FBYTtJQUt4QixxQ0FBZ0IsYUFBQyxLQUFhO0lBSzlCLHlDQUFvQixhQUFDLEtBQWE7SUFLbEMsMkNBQXNCLGFBQUMsS0FBYTtJQUtwQyxvQ0FBZSxhQUFDLEtBQWE7SUFLN0IsbUNBQWMsYUFBQyxLQUFhO0lBSzVCLDhDQUF5QixhQUFDLEtBQWE7SUFLdkMsa0NBQWEsYUFBQyxLQUFhO0lBSzNCLGlDQUFZLGFBQUMsS0FBYTtJQUsxQixzQ0FBaUIsYUFBQyxLQUFhO0lBSy9CLDJDQUFzQixhQUFDLEtBQWE7SUFLcEMseUNBQW9CLGFBQUMsS0FBYTtJQUtsQyw0Q0FBdUIsYUFBQyxLQUFhO0lBS3JDLHVCQUFFLGFBQUMsS0FBYTs7Ozs7Ozs7cUJBakZwQjtFQWNnQyxpQkFBaUI7U0FBcEMsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBsdWdpbiwgQ29yZG92YSwgSW9uaWNOYXRpdmVQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG5AUGx1Z2luKHtcbiAgcGx1Z2luTmFtZTogJ0Jpek1PQlB1c2gnLFxuICBwbHVnaW46ICdiaXptb2ItcGx1Z2luLXB1c2gnLFxuICBwbHVnaW5SZWY6ICdCaXpNT0JQdXNoJyxcbiAgcmVwbzogJycsXG4gIGluc3RhbGw6ICcnLFxuICBpbnN0YWxsVmFyaWFibGVzOiBbXSxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnLCAnaU9TJ11cbn0pXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBCaXpNT0JQdXNoIGV4dGVuZHMgSW9uaWNOYXRpdmVQbHVnaW4ge1xuICAgIEBDb3Jkb3ZhKClcbiAgICBnZXRQdXNoS2V5KHBhcmFtOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIEBDb3Jkb3ZhKClcbiAgICBwdXNoUmVnaXN0cmF0aW9uKHBhcmFtOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIEBDb3Jkb3ZhKClcbiAgICBwdXNoQWxhcm1TZXR0aW5nSW5mbyhwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgcHVzaFVwZGF0ZUFsYXJtU2V0dGluZyhwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgcHVzaEdldE1lc3NhZ2VzKHBhcmFtOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIEBDb3Jkb3ZhKClcbiAgICBwdXNoTWFya0FzUmVhZChwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgcHVzaEdldFVucmVhZE1lc3NhZ2VDb3VudChwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgc2V0QmFkZ2VDb3VudChwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgc2VuZE1lc3NhZ2VzKHBhcmFtOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIEBDb3Jkb3ZhKClcbiAgICBwdXNoUmVjZWl2ZWRDaGVjayhwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgZ2V0UHVzaE1lc3NhZ2VUeXBlTGlzdChwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBAQ29yZG92YSgpXG4gICAgZ2V0UHVzaEFncmVlbWVudEluZm8ocGFyYW06IG9iamVjdCk6IFByb21pc2U8YW55PiB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgQENvcmRvdmEoKVxuICAgIHVwZGF0ZVB1c2hBZ3JlZW1lbnRJbmZvKHBhcmFtOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIEBDb3Jkb3ZhKClcbiAgICBvbihwYXJhbTogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgIHJldHVybjtcbiAgICB9XG59XG4iXX0=