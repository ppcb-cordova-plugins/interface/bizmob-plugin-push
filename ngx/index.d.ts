import { IonicNativePlugin } from '@ionic-native/core';
export declare class BizMOBPush extends IonicNativePlugin {
    getPushKey(param: object): Promise<any>;
    pushRegistration(param: object): Promise<any>;
    pushAlarmSettingInfo(param: object): Promise<any>;
    pushUpdateAlarmSetting(param: object): Promise<any>;
    pushGetMessages(param: object): Promise<any>;
    pushMarkAsRead(param: object): Promise<any>;
    pushGetUnreadMessageCount(param: object): Promise<any>;
    setBadgeCount(param: object): Promise<any>;
    sendMessages(param: object): Promise<any>;
    pushReceivedCheck(param: object): Promise<any>;
    getPushMessageTypeList(param: object): Promise<any>;
    getPushAgreementInfo(param: object): Promise<any>;
    updatePushAgreementInfo(param: object): Promise<any>;
    on(param: object): Promise<any>;
}
